# latinum

__NOTE__: Since __2021-11-24__, This repository has been superceded by a newer one available at https://git.dax.one/visualfabric/latinum.

## Generating Javascript documentation.

The source code for Latinum has been annotated with inline comments that conform to the syntax
understood by JSDoc. In order to generate the documentation, install JSDoc and then execute the
following command at a terminal:

```
jsdoc lib -c jsdoc.json -d apidocs -R README.md
```